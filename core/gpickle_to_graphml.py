import networkx as nx
import lxml

G = nx.read_gpickle('dbt_project/target/graph.gpickle')
for (n1,attr) in G.nodes(data=True):
    attr.clear()
for (n1, n2, attr) in G.edges(data=True):
    attr.clear()
nx.write_graphml_lxml(G, "dbtdag.graphml")
