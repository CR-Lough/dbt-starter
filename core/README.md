```mermaid
flowchart LR;
model.dbt_project.my_first_dbt_model-->model.dbt_project.my_second_dbt_model
model.dbt_project.my_first_dbt_model-->test.dbt_project.unique_my_first_dbt_model_id.16e066b321
model.dbt_project.my_first_dbt_model-->test.dbt_project.not_null_my_first_dbt_model_id.5fb22c2710
model.dbt_project.my_second_dbt_model-->test.dbt_project.unique_my_second_dbt_model_id.57a0f8c493
model.dbt_project.my_second_dbt_model-->test.dbt_project.not_null_my_second_dbt_model_id.151b76d778
```

