FROM python:3.8-slim

RUN useradd -r -m -U dbtuser

#variables will exist on local machine in '.env'
#to be passed into project at runtime
ENV DBT_PROFILES_DIR='core/dbt_project'

WORKDIR /core

COPY --chown=dbtuser:dbtuser ./requirements.txt .
RUN pip install -r requirements.txt --no-cache-dir && \
    rm requirements.txt

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential && \
    apt-get install -y --reinstall xdg-utils && \
    apt-get install -y --no-install-recommends xml-twig-tools

COPY --chown=dbtuser:dbtuser core/ /core/
WORKDIR /core/dbt_project

USER dbtuser
CMD dbt deps && exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
