# dbt-starter


## Name
Starter kit for all dbt-Snowflake projects via CLI. Uses Python:3.8-slim as base Docker image. 

## Description
This starter kit takes care of the boilerplate actions that must be done when starting a new dbt project. For example, initializing the new dbt project, setting up your profiles.yml, and adding packages from dbt-hub (add to core/dbt_project/packages.yml) or pypi (add to requirements.txt).

The project also includes CI that performs SQL linting, and pushes your project's Docker image to the GitLab container repository. 

This serves as an alternative to the dbt base Docker image. 

## Installation
```
cd folder_to_hold_repo
git clone git@gitlab.com:CR-Lough/dbt-starter.git
```
- Create 'dbt-starter/.env' and populate with your Snowflake environment variables (listed in 'dbt-starter/core/dbt_project/profiles.yml)
```
docker compose up --build 
docker ps
docker exec -it <container id> /bin/bash 
```
- Start running dbt commands from the container's 'dbt_project' directory level

## Usage

## Support
- [dbt Slack community](https://www.getdbt.com/community/join-the-community/)
- [dbt Hub - packages](https://hub.getdbt.com/)
- [dbt - How to build a mature dbt project from scratch](https://docs.getdbt.com/blog/how-to-build-a-mature-dbt-project-from-scratch)
- [dbt - Maturity repository](https://github.com/dbt-labs/dbt-project-maturity)
- [How dbt labs structures their projects](https://discourse.getdbt.com/t/how-we-structure-our-dbt-projects/355)

## Roadmap
1. Add quick Loom video to Usage section
2. Add SQLFluff configuration and incorporate into CI
