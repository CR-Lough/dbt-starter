networkx==2.8.7
lxml==4.9.1
dbt-snowflake==1.3.0
dbt-core==1.3.0
pre-commit==2.18.1
sqlfluff==1.3.2